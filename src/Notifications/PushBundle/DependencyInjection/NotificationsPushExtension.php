<?php

namespace Notifications\PushBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class NotificationsPushExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('notifications_push.platforms', $config['platforms']);
        $container->setParameter('notifications_push.key', $config['aws']['key']);
        $container->setParameter('notifications_push.secret', $config['aws']['secret']);
        $container->setParameter('notifications_push.region', $config['aws']['region']);
        $container->setParameter('notifications_push.debug', $config['debug']);


    }
}
