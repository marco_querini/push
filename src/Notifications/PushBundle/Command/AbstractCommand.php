<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 09/07/15
 * Time: 15.28
 */

namespace Notifications\PushBundle\Command;



use Doctrine\ORM\EntityManager;
use Notifications\PushBundle\Service\SnsService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

abstract class AbstractCommand  extends ContainerAwareCommand {

    /**
     * @return SnsService
     */
    public function getPushNotificationService(){

        return $this->getContainer()->get('notifications_push.sns_service');

    }

    /**
     * @return EntityManager
     */
    public function getEntityManager(){

        return $this->getContainer()->get('doctrine.orm.entity_manager');

    }
}
