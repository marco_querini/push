<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 13/07/15
 * Time: 16.32
 */

namespace Notifications\PushBundle\Tests\Controller;

use Notifications\PushBundle\Model\Message;
use Notifications\PushBundle\Service\SnsService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PublishTest extends WebTestCase {
    /** @var  ContainerInterface */
    private $container;

    /** @var SnsService */
    private $snsService;

    public function setUp()
    {


        parent::SetUp();
        $kernel = static::createKernel();
        $kernel->boot();
        $this->container = $kernel->getContainer();
        $this->snsService = $this->container->get('notifications_push.sns_service');


    }

    public function testPublish()
    {

        $this->snsService->awsPublish(new Message("testMessage"), 'arn:aws:sns:us-west-2:909555167046:FirstAmazonTestTopic');

    }
}