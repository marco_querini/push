<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 13/07/15
 * Time: 15.50
 */

namespace Notifications\PushBundle\Tests\Controller;

use Notifications\PushBundle\Service\SnsService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use Aws\Sns\SnsClient;


class CreateTopicTest extends WebTestCase {

    /** @var  ContainerInterface */
    private $container;

    /** @var SnsService */
    private $snsService;

    public function setUp()
    {


        parent::SetUp();
        $kernel = static::createKernel();
        $kernel->boot();
        $this->container = $kernel->getContainer();
        $this->snsService = $this->container->get('notifications_push.sns_service');


    }

    public function testCreateTopic()
    {

        $androidPlatform = $this->container->getParameter('notifications_push.platforms')['android'];

        $this->snsService->awsCreateTopic("testTopic");

    }

}