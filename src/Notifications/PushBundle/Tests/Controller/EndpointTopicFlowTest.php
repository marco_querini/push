<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 14/07/15
 * Time: 12.09
 */

namespace Notifications\PushBundle\Tests\Controller;

use Notifications\PushBundle\Model\Message;
use Notifications\PushBundle\Service\SnsService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class EndpointTopicFlowTest extends WebTestCase {
    /** @var  ContainerInterface */
    private $container;

    /** @var SnsService */
    private $snsService;

    public function setUp()
    {
        parent::SetUp();
        $kernel = static::createKernel();
        $kernel->boot();
        $this->container = $kernel->getContainer();
        $this->snsService = $this->container->get('notifications_push.sns_service');
    }

    public function testEndpointTopicFlow()
    {

        $uuid =  uniqid();

        $androidPlatform = $this->container->getParameter('notifications_push.platforms')['android'];

        $topicArn = $this->snsService->awsCreateTopic("awsTestTopic-".$uuid);

        $deviceEndpointArn = $this->snsService->awsCreatePlatformEndpoint("fr6ZEqtETjc:APA91bElTM1o6GMKoz5WLQV-5KiryRyPerJH_RSn0REW2HjeZouMKg-X01dR5xLV8M4ggrBaXlBgCVyMrfa-F3iZ7ARV_Mu4_03HipiZEYjYe7oM9alS1xxEqTSBsG36ZRN-1I04cNLk",$androidPlatform );

        $this->snsService->subscribeEndpointToTopic($deviceEndpointArn, $topicArn);

        $this->snsService->subscribeEndpointToTopic('marco.querini@trustmyphone.com', $topicArn, 'email');

        $this->snsService->awsPublish(new Message("testEndpointTopicFlowMessage"), $topicArn);


    }
}