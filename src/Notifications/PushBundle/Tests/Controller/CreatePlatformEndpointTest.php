<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 10/07/15
 * Time: 17.00
 */

namespace Notifications\PushBundle\Tests\Controller;

use Notifications\PushBundle\Service\SnsService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use Aws\Sns\SnsClient;



class CreatePlatformEndpointTest extends WebTestCase {

    /** @var  ContainerInterface */
    private $container;

    /** @var SnsService */
    private $snsService;

    public function setUp()
    {


        parent::SetUp();
        $kernel = static::createKernel();
        $kernel->boot();
        $this->container = $kernel->getContainer();
        $this->snsService = $this->container->get('notifications_push.sns_service');


    }

    public function testCreatePlatformEndpoint()
    {

        $androidPlatform = $this->container->getParameter('notifications_push.platforms')['android'];

        $this->snsService->awsCreatePlatformEndpoint("fr6ZEqtETjc:APA91bElTM1o6GMKoz5WLQV-5KiryRyPerJH_RSn0REW2HjeZouMKg-X01dR5xLV8M4ggrBaXlBgCVyMrfa-F3iZ7ARV_Mu4_03HipiZEYjYe7oM9alS1xxEqTSBsG36ZRN-1I04cNLk",$androidPlatform );






    }

}