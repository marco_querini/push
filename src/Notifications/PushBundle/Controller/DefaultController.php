<?php

namespace Notifications\PushBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('NotificationsPushBundle:Default:index.html.twig', array('name' => $name));
    }
}
