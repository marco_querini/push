<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 10/07/15
 * Time: 15.16
 */

namespace Notifications\PushBundle\Service;

use Aws\Sns\Exception\InvalidParameterException;
use Aws\Sns\SnsClient;
use Monolog\Logger;
use Notifications\PushBundle\Exception\MessageTooLongException;
use Notifications\PushBundle\Model\Message;

class SnsService {

    /**
     * @var bool
     */
    private $debug;

    /**
     * @var SnsClient
     */
    private $sns;

    /**
     * @var Logger
     */
    private $logger;
    /**
     * @param SnsClient $client
     * @param Logger $logger
     * @param $debug
     */
    public function __construct(SnsClient $client, Logger $logger, $debug)
    {
        $this->sns = $client;
        $this->logger = $logger;
        $this->debug = $debug;
    }

    /**
     * Register a device token
     *
     * @param string $deviceToken device token
     * @param string $platform platform on which to register
     * @return string the endpoint ARN for this device
     */
    public function awsCreatePlatformEndpoint($deviceToken, $platformArn)
    {
        try {
            $res = $this->sns->createPlatformEndpoint(
                [
                    'PlatformApplicationArn' => $platformArn,
                    'Token' => $deviceToken,
                    'Attributes' => [
                        'Enabled' => 'true'
                    ]
                ]
            );

            if ($this->debug) $this->logger->info("Device Token: ".$deviceToken." registered on Platform(arn): ".$platformArn);

        } catch (InvalidParameterException $e) {

            preg_match('/Endpoint (.+?) already/', $e->getMessage(), $matches);

            if (isset($matches[1])) {
                $this->sns->setEndpointAttributes(
                    [
                        'EndpointArn' => $matches[1],
                        'Attributes' => [
                            'Enabled' => 'true'
                        ]
                    ]
                );

                if ($this->debug) $this->logger->warning("Device Token: ".$deviceToken." already registered on Platform[arn]: ".$platformArn);
                return $matches[1];
            } else {
                if ($this->debug) $this->logger->error($e->getMessage());
                throw $e;
            }
        }
        return $res['EndpointArn'];
    }




    /**
     * Create a Topic
     *
     * @param string $name topic name
     * @return string the endpoint ARN for this device
     */
    public function awsCreateTopic($topicName)
    {
        try {
            $res = $this->sns->createTopic(
                [
                    'Name' => $topicName
                ]
            );

            if ($this->debug) $this->logger->info("Topic: ".$topicName." created: ".$res['TopicArn']);

        } catch (\Exception $e) {

            if ($this->debug) $this->logger->error($e->getMessage());

            throw $e;

        }
        return $res['TopicArn'];
    }


    /**
     * Send a message to an endpoint
     *
     * @param Message|string $message
     * @param string $endpointArn
     * @throws MessageTooLongException
     */
    public function awsPublish($message, $endpointArn)
    {

        if (!($message instanceof Message)) {
            $message = new Message($message);
        }


        try {

            $this->sns->publish(
                [
                    'TargetArn' => $endpointArn,
                    'Message' => $this->encodeMessage($message),
                    'MessageStructure' => 'json'
                ]
            );


            if ($this->debug) {
                $this->logger->info(
                    "Message was sent to $endpointArn",
                    [
                        'Message' => $message
                    ]
                );
            }

        } catch (\Exception $e) {
            if ($this->debug) $this->logger->error(
                "Failed to push to {$endpointArn}",
                [
                    'Message' => $message,
                    'Exception' => $e,
                    'Endpoint' => $endpointArn
                ]
            );

            throw $e;
        }
    }




    /**
     * Subscribe a device to the topic, will create new numbered topics
     * once the first is full
     *
     * @param string $deviceEndpointArn
     * @param string $topicArn The base name of the topics to use
     */
    public function subscribeEndpointToTopic($deviceEndpointArn, $topicArn, $protocol='application')
    {

        try{

            $res = $this->sns->subscribe(
                [

                    'TopicArn' => $topicArn,
                    'Protocol' => $protocol,
                    'Endpoint' => $deviceEndpointArn
                ]
            );


            if ($this->debug) $this->logger->error(
                "Subscribed ".$deviceEndpointArn." to ".$topicArn,
                [
                    'TopicArn' => $topicArn,
                    'Protocol' => $protocol,
                    'Endpoint' => $deviceEndpointArn
                ]
            );

        }catch (\Exception $e){

            if ($this->debug) $this->logger->error(
                "Failed to subscribe ".$deviceEndpointArn." to ".$topicArn,
                [
                    'TopicArn' => $topicArn,
                    'Protocol' => $protocol,
                    'Endpoint' => $deviceEndpointArn
                ]
            );

            throw $e;


        }

        return $res['SubscriptionArn'];


    }



    public function unsubscribeDeviceToTopic($subscriptionArn)
    {
        $this->sns->unsubscribe(array('SubscriptionArn' => $subscriptionArn));
    }

    /**
     * @param string $topicArn
     * @param string | null $nextToken
     * @return array
     */
    public function getSubscriptionsByTopic($topicArn, $nextToken = null)
    {
        $endpoints  = array();
        $parameters = array();
        $parameters['TopicArn'] = $topicArn;

        if($nextToken !== null){
            $parameters['NextToken'] = $nextToken;
        }

        $currentSubscriptions = $this->sns->listSubscriptionsByTopic($parameters);

        foreach($currentSubscriptions['Subscriptions'] as $endpoint){
            $endpoints[] = $endpoint['Endpoint'];
        }

        if(isset($currentSubscriptions['NextToken']) && $currentSubscriptions['NextToken'] != null){
            $endpoints = array_merge($endpoints, $this->getSubscriptionsByTopic($topicArn, $currentSubscriptions['NextToken']));
        }

        return $endpoints;
    }



    /**
     * @param Message $message
     * @return string
     * @throws MessageTooLongException
     */
    private function encodeMessage(Message $message)
    {
        try {
            $json = json_encode($message, JSON_UNESCAPED_UNICODE);
            return $json;
        } catch (\Exception $e) {
            if ($e->getPrevious() instanceof MessageTooLongException) {
                if ($this->debug) $this->logger->error($e->getPrevious()->getMessage());
                throw $e->getPrevious();
            }

            if ($this->debug) $this->logger->error($e->getMessage());
            throw $e;
        }
    }










}
